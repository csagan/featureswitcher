﻿using System;
using Switcheroo;
using Switcheroo.Toggles;

namespace FeatureSwitcher
{
    public class Program
    {
        static void Main(string[] args)
        {
            //Features.Initialize(x => x.FromApplicationConfig());
            Console.Write("Role: ");
            var userrole = Console.ReadLine();
            userrole = userrole?.Trim();

            IFeatureConfiguration features = new FeatureConfiguration
            {
                new BooleanToggle("Feature1", userrole == "admin")

            };

            Console.WriteLine($"Feature is {(features.IsEnabled("Feature1") ? "enabled" : "disabled")}.");


            Console.ReadKey();
        }
    }
}
